<?php

/**
 * Low Likes config file
 *
 * @package        readunread
 * @author         Stuart McDonALD
 * @link           https://twitter.com/stumcd69
 * @copyright      Copyright (c) 2013, Stu
 */

if ( ! defined('READUNREAD_NAME'))
{
	define('READUNREAD_NAME',    'Read Unread');
	define('READUNREAD_PACKAGE', 'readunread');
	define('READUNREAD_VERSION', '0.0.5');
	define('READUNREAD_DOCS',    'http://twitter.com/stumcd69');
}