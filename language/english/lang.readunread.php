<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(	
	'readunread_module_name' => 
	'ReadUnread',

	'readunread_module_description' => 
	'Auto Remove Favourites when called',

	'module_home' => 'ReadUnread Home',
	
// Start inserting custom language keys/values here
	
);

/* End of file lang.readunread.php */
/* Location: /system/expressionengine/third_party/readunread/language/english/lang.readunread.php */
