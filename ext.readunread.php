<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include config file
include(PATH_THIRD.'readunread/config.php');

/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2003 - 2011, EllisLab, Inc.
 * @license		http://expressionengine.com/user_guide/license.html
 * @link		http://expressionengine.com
 * @since		Version 2.0
 * @filesource
 */
 
// ------------------------------------------------------------------------

/**
 * ReadUnread Extension
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Extension
 * @author		Stuart McDonald
 * @link		https://twitter.com/stumcd69
 */

class Readunread_ext {
/**
 * Name, required for Extensions page
	 *
	 * @access      public
	 * @var         string
	 */
	public $name = READUNREAD_NAME;

	/**
	 * Docs URL, required for Extensions page
	 *
	 * @access      public
	 * @var         string
	 */
	public $docs_url = READUNREAD_DOCS;

	/**
	 * This version
	 *
	 * @access      public
	 * @var         string
	 */
	public $version = READUNREAD_VERSION;
	
	public $settings 		= array();
	/*public $description		= 'Select the Solspace Favourite Collection';
	public $docs_url		= '';
	public $name			= 'ReadUnread';*/
	public $settings_exist	= 'n';

	public $required_by		= array('module');

		
	private $EE;
	
	/**
	 * Constructor
	 *
	 * @param 	mixed	Settings array or empty string if none exist.
	 */
	public function __construct($settings = array())
	{
		$this->EE =& get_instance();
		//$this->settings = $settings;
	}
	
	// ----------------------------------------------------------------------
	
	/**
	 * Settings Form
	 *
	 * If you wish for ExpressionEngine to automatically create your settings
	 * page, work in this method.  If you wish to have fine-grained control
	 * over your form, use the settings_form() and save_settings() methods 
	 * instead, and delete this one.
	 *
	 * @see http://expressionengine.com/user_guide/development/extensions.html#settings
	 */
	public function settings()
	{
		return array(
			
		);
	}
	
	// ----------------------------------------------------------------------
	
	/**
	 * Activate Extension
	 *
	 * This function enters the extension into the exp_extensions table
	 *
	 * @see http://codeigniter.com/user_guide/database/index.html for
	 * more information on the db class.
	 *
	 * @return void
	 */
		/*public function activate_extension()
		{
			// Setup custom settings in this array.
			$this->settings = array();
			
			$hooks = array(
				'channel_form_submit_entry_end'	=> 'new_entry_end',
				'delete_comment_additional'	=> 'delete_comment',
				'insert_comment_end' => 'new_comment',
			);
	
			foreach ($hooks as $hook => $method)
			{
				$data = array(
					'class'		=> __CLASS__,
					'method'	=> $method,
					'hook'		=> $hook,
					'settings'	=> serialize($this->settings),
					'version'	=> $this->version,
					'enabled'	=> 'y'
				);
	
				$this->EE->db->insert('extensions', $data);			
			}
		}	*/

	// ----------------------------------------------------------------------
	
	/**
	 * new_entry_end
	 *
	 * @param 
	 * @return 
	 */
	public function channel_form_submit_entry_end($sd)
	{
		// Add Code for the entry_submission_absolute_end hook here.
		//
		// Remove all entries in Collection (4) that have this entry_id
		//
		
		$actualID	= (isset($sd->entry['entry_id']));
		$theAuthor 	= (isset($sd->entry['author_id']));
		// --------------------------------
		//Set Favorite group (Hardcoded atm)
		// --------------------------------
		$fav_group = '4';
		//
		// Grab a list of all members active in the last 2 weeks (not guests) + Exclude the AUTHOR
		//
		$members = $this->EE->db->select('member_id, last_activity')
			->from('members')
			->where('(from_unixtime(last_activity) > CURDATE() - INTERVAL 6 WEEK)')
			->where ('group_id >', '3')
			->where ('member_id <>', $theAuthor)
			->get();
		
		if ($members->num_rows == 0)
		{
			exit('No members found');
		}
		//Create array for looping
		$member_ids = array();
		foreach ($members->result() AS $row) {
			$member_ids[] = $row->member_id;
		}
		//
		// Add new rows to the DB collection_id (#4), favouriter_id (user_id), type (entry_id), author_id (The author), entry_id (0), item_id (entry_id), site_id (1), favourited_date (unixtime)
		//
		if (count ($member_ids) > 0)
		{
			$data = array();
			foreach ($member_ids AS $value) {
				 $data[] = array(
					'favorites_id' => '',
					'collection_id' => $fav_group,
					'favoriter_id' => $value,//$this->EE->session->userdata('member_id'),
					'type' => 'entry_id',
					'author_id' => $theAuthor,
					'entry_id' =>'',
					'item_id'=> $actualID,
					'site_id' => '1',
					'favorited_date' => $this->EE->localize->now,
					'notes' => 'added on new entry'
				);
			}
		}
		// --------------------------------
		$this->EE->db->insert_batch('favorites', $data);
	}

	// ----------------------------------------------------------------------
	
	/**
	 * delete_comment
	 *
	 * @param 
	 * @return 
	 */
	public function delete_comment_additional()
	{
		// Add Code for the delete_comment_additional hook here.
		//$this->EE->TMPL->log_item('ReadUnread: someone deleted a comment');
	}

	// ----------------------------------------------------------------------
	
	/**
	 * new_comment
	 *
	 * @param  int  $entry_id
	 * @param 
	 * @return 
	 */
	public function insert_comment_end( $data, $comment_moderate, $comment_id )
	{
		// Add Code for the comment_form_end hook here.
		
		// --------------------------------
		// Grab the comment ID and look up which entry its related to
		// --------------------------------
		$entryID = $this->EE->db->select('entry_id, author_id')
			->from('comments')
			->where('comment_id', $comment_id)
			->get();
		
		if ($entryID->num_rows() == 0)
		{
		    exit('No entryID found');
		}
		// --------------------------------
		//Set var to EntryID
		// --------------------------------
		$actualID = $entryID->row('entry_id');
		// --------------------------------
		//Set author variable
		// --------------------------------
		$theAuthor = $entryID->row('author_id');
		// --------------------------------
		//Set Favorite group (Hardcoded atm)
		// --------------------------------
		$fav_group = '4';
		
		// --------------------------------
		//Get array of members active in the last 2 weeks
		// --------------------------------
		$members = $this->EE->db->select('member_id, last_activity')
			->from('members')
			->where('(from_unixtime(last_activity) > CURDATE() - INTERVAL 6 WEEK)')
			->where ('group_id >', '3')
			->where ('member_id <>', $this->EE->session->userdata('member_id'))
			->get();
		
		if ($members->num_rows == 0)
		{
			exit('No members found');
		}
		//Create array for looping
		$member_ids = array();
		foreach ($members->result() AS $row) {
			$member_ids[] = $row->member_id;
		}
		//Test we have members
		//$memberCount = count ($member_ids);
		
		// --------------------------------
		// Create array that holds item_id + group for deletion
		// --------------------------------
		$toDelete = array(
			'item_id' 		=> $actualID,
			'collection_id' => $fav_group//,
			//'favoriter_id' => $this->EE->session->userdata('member_id')
		);
		//Entry or not
		//$this->EE->db->where($toDelete);
		//$doesExist = $this->EE->db->count_all_results('favorites');
		//Count is more then 0
		//if ($doesExist)
		//{
		
		// --------------------------------
		//Remove ALL entries where item_id + collection_id == the same
		// --------------------------------
		$this->EE->db->delete('favorites', $toDelete);
		//}
		//else 
		//{
			// --------------------------------
			//Replace the deleted entries with updated entry_ids + member_ids
			// --------------------------------
			if (count ($member_ids) > 0)
			{
				$data = array();
				foreach ($member_ids AS $value) {
					 $data[] = array(
						'favorites_id' => '',
						'collection_id' => $fav_group,
						'favoriter_id' => $value,//$this->EE->session->userdata('member_id'),
						'type' => 'entry_id',
						'author_id' => $theAuthor,
						'entry_id' =>'',
						'item_id'=> $actualID,
						'site_id' => '1',
						'favorited_date' => $this->EE->localize->now,
						'notes' => 'added on comment submit'
					);
				}
			}
			/*$data = array(
				'favorites_id' => '',
				'collection_id' => $fav_group,
				'favoriter_id' => $this->EE->session->userdata('member_id'),
				'type' => 'entry_id',
				'author_id' => $theAuthor,
				'entry_id' =>'',
				'item_id'=> $actualID,
				'site_id' => '1',
				'favorited_date' => $this->EE->localize->now,
				'notes' => $memberCount 
			);*/
			// --------------------------------
			$this->EE->db->insert_batch('favorites', $data);
		//}
		// --------------------------------
		//$this->EE->db->delete($actualID)
		//	->from ('favorites')
		//	->where ('item_id', $actualID)
		//	->and ('collection_id', '4');
		
		// --------------------------------
		// Grab a list of all members active in the last 2 weeks (not guests)
		// --------------------------------
		
		// --------------------------------
		// Delete rows in the DB where entry_id = parent entry ID + collection = 4
		// --------------------------------
		// Add new rows to the DB collection_id (#4), favoriter_id (user_id), type (entry_id), author_id (The author), entry_id (0), item_id (entry_id), site_id (1), favourited_date (unixtime)
		
		//if (AJAX_REQUEST)
		//{
		//	die();
		//}
		
		//$this->EE->TMPL->log_item('ReadUnread: someone posted a comment');
	}


	// ----------------------------------------------------------------------

	/**
	 * Disable Extension
	 *
	 * This method removes information from the exp_extensions table
	 *
	 * @return void
	 */
	/*function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}
	*/
	
}

/* End of file ext.readunread.php */
/* Location: /system/expressionengine/third_party/readunread/ext.readunread.php */