# Dependancies #

This module uses Solspaces 'favourite' module and it's DB table to record favourites and tap into their already built tags.
It has the collection ID hard coded - so be careful

### What is this repository for? ###

* Publicly share my code so I may learn from feedback

### How do I get set up? ###

* EE
* Solspace '[Favorites](http://www.solspace.com/software/detail/favorites/)' installed
* Create a new collection in 'Favorites' (type is entry_id)
* Enable module
* The actual Favorites collection ID is hard coded (need to make a simple CP dropdown to make it more versatile)
* No DB config
* Use the {exp:readunread:markasread entryID="x"} tag on single view templates to remove the users 'favourite' making it 'read'
* Hooks only exists for comments and new entries submitted on the front end (comments + channel form)
* It only adds users to the DB who have been active in the last 2 weeks and are not guests (needs work)

### Contribution guidelines ###

* Point out any relevant improvements
* Pull it to pieces, this is my first module/extension
* Be nice :)

### Who do I talk to? ###

* Hit me up via Twitter (stuartmcd69)