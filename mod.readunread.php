<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include config file
include(PATH_THIRD.'readunread/config.php');

/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2003 - 2011, EllisLab, Inc.
 * @license		http://expressionengine.com/user_guide/license.html
 * @link		http://expressionengine.com
 * @since		Version 2.0
 * @filesource
 */
 
// ------------------------------------------------------------------------

/**
 * ReadUnread Module Front End File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Stuart McDonald
 * @link		https://twitter.com/stumcd69
 */

class Readunread {
	
	//public $return_data;
	/**
	 * EE Superobject
	 *
	 * @access      private
	 * @var         object
	 */
	private $EE;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
	}
	
	// ----------------------------------------------------------------

	/**
	 * Start on your custom code here...
	 */
	public function markasread()
	{
		//$entry_id = $this->EE->TMPL->fetch_param('entry_id');
		$entry_id = ee()->TMPL->fetch_param('entryID');
		if (empty($entry_id))
		{
			// Make a note of it in the template log
			//$this->EE->TMPL->log_item('Readunread: no entry_id given in markasread tag');
			//die();
			return $tagdata;
		}
		$member_id = $this->EE->session->userdata('member_id');
		
		//$this->EE->TMPL->log_item($entry_id);
		
		// --------------------------------------
		// Delete this members entry from the Fav table where entry_id = item_id + member_id = favoriter_id
		// --------------------------------------
		if ($member_id && $entry_id)
		{
			// Set the array of the item_id + favoriter_id
			$data = array(
				'item_id' 		=> $entry_id,
				'favoriter_id'	=> $member_id,
				'collection_id'	=> '4'
			);
			//Delete from Favorites Table
			$this->EE->db->delete('favorites', $data);
		}
		
		return;
	}
}
/* End of file mod.readunread.php */
/* Location: /system/expressionengine/third_party/readunread/mod.readunread.php */